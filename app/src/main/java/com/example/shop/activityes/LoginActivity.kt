package com.example.shop.activityes

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.example.shop.R
import com.example.shop.dataloaders.ApiRequest
import com.example.shop.dataloaders.MainApiRequest
import com.example.shop.tools.Tools
import com.example.shop.extressions.setColor
import com.example.shop.interfaces.CallbackApi
import com.example.shop.sharedpreferences.Sharedpreference
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.loader_layout.*
import org.json.JSONObject

class LoginActivity : AppCompatActivity() {

    val map= mutableMapOf<String,String>()
    var clicked:Boolean=false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        init()
    }
    private fun init(){
        setspanable()
        clicks()
        checkmail()
    }

    private fun setspanable(){
        signup.setColor(getString(R.string.new_user)+" ",ContextCompat.getColor(this,
            R.color.maincolortextview
        ))
        signup.setColor(getString(R.string.sign_up)+" ",ContextCompat.getColor(this,
            R.color.colorPrimary
        ))
        signup.setColor(getString(R.string.here),ContextCompat.getColor(this,
            R.color.maincolortextview
        ))
    }

    private fun clicks(){
        rememberme.setOnClickListener(){
            if(check.drawable.constantState == ContextCompat.getDrawable(this,
                    R.mipmap.ic_check_circle_outline
                )!!.constantState) check.setImageResource(R.mipmap.ic_radio_button_unchecked)
            else{
                check.setImageResource(R.mipmap.ic_check_circle_outline)
                clicked=true
            }
        }
        signinbtn.setOnClickListener(){

            val mail=email.text.toString()
            val password=passwordtext.text.toString()
            if(mail.isEmpty() || password.isEmpty())
            Tools().showdialog(this,getString(R.string.all_fields_are_required))
            else if(!Tools().isEmailValid(mail))
                Tools().showdialog(this,getString(
                    R.string.e_mail_is_not_valid
                ))
            else{
                spinkit.visibility=View.VISIBLE
                map["email"]=mail
                map["Password"]=password
                MainApiRequest.postRequest(MainApiRequest.LOGIN,map,object :CallbackApi{
                    override fun onresponse(reponse: String) {
                        spinkit.visibility=View.GONE
                        val jsobj=JSONObject(reponse)
                        if(jsobj.has("OK")){
                            if(!jsobj.getBoolean("OK")){
                                Tools().showdialog(this@LoginActivity,jsobj.getString("error"))
                            }
                        }
                        else if(jsobj.has("user_id")){
                            if(clicked) Sharedpreference.instance().save(Sharedpreference.USER,jsobj.getString("user_id"))
                            Sharedpreference.instance().save(Sharedpreference.TOKEN,jsobj.getString("token"))
                            Sharedpreference.instance().save(Sharedpreference.USER_ID,jsobj.getString("user_id"))
                            checkprofile(jsobj.getString("user_id"))

                        }
                     }

                    override fun onfailure(error: String) {
                        spinkit.visibility=View.GONE
                     }
                },this)
            }
        }
        signup.setOnClickListener(){
            val intent=Intent(this, RegisterActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out)
        }

    }

    private fun  checkprofile(uid:String){
        val params= mutableMapOf<String,String>()
        params["user_id"]=uid
        lateinit var  activity: Activity
        MainApiRequest.postRequest("profile",params,object :CallbackApi{
            override fun onresponse(reponse: String) {
                val jsobj=JSONObject(reponse)
                var bool=jsobj.has("profile-completed")
                if(jsobj.has("profile-completed")){
                    if(!jsobj.getBoolean("profile-completed"))
                        activity=CompleteProfileActivity()
                }
                else
                activity=DashboardActivity()

                val intent=Intent(this@LoginActivity,activity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                startActivity(intent)
                overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out)
             }

            override fun onfailure(error: String) {
             }
        },this)
    }
    private fun checkmail(){
        email.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {


             }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {


             }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

               if(Tools().isEmailValid(s.toString())) emailicon.visibility=View.VISIBLE
                else emailicon.visibility=View.INVISIBLE
             }
        })
    }
}

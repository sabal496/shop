package com.example.shop.activityes

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log.d
import android.view.View
import androidx.core.content.ContextCompat
import com.example.shop.R
import com.example.shop.dataloaders.MainApiRequest
import com.example.shop.tools.Tools
import com.example.shop.extressions.setColor
import com.example.shop.interfaces.CallbackApi
import kotlinx.android.synthetic.main.activity_register.*
import org.json.JSONObject

class RegisterActivity : AppCompatActivity() {

    val map= mutableMapOf<String,String>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        init()
    }

    private fun init(){
        setspanable()
        checkmai()
        clicks()
    }

    private fun setspanable(){
        loginlinlk.setColor(getString(R.string.allready_a_mamber),ContextCompat.getColor(this,
            R.color.maincolortextview
        ))
        loginlinlk.setColor(getString(R.string.log_in),ContextCompat.getColor(this,
            R.color.colorPrimary
        ))
    }

    private fun checkmai(){
        registermail.addTextChangedListener(object :TextWatcher{
            override fun afterTextChanged(s: Editable?) {
             }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
             }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (Tools().isEmailValid(s.toString()))mailvalidlogo.visibility=View.VISIBLE
                else mailvalidlogo.visibility=View.INVISIBLE

             }
        })
    }

    private fun clicks(){
        signupbtn.setOnClickListener(){
            val mail=registermail.text.toString()
            val  fullname=registername.text.toString()
            val password=registerpassword.text.toString()
            val reppassword=repeatpassword.text.toString()
            if(mail.isEmpty() || fullname.isEmpty() || password.isEmpty() || reppassword.isEmpty())
                Tools().showdialog(this,getString(
                    R.string.all_fields_are_required
                ))
            else if(!Tools().isEmailValid(mail))
                Tools().showdialog(this,getString(
                    R.string.e_mail_is_not_valid
                ))
            else if (password!=reppassword) Tools().showdialog(this,getString(
                R.string.password_dont_match
            ))

            else{
                map["email"]=mail
                map["password"]=password
                map["full_name"]=fullname
                MainApiRequest.postRequest(MainApiRequest.REGISTER,map,object : CallbackApi{
                    override fun onresponse(reponse: String) {
                        val jsonobj=JSONObject(reponse)
                        if(jsonobj.has("OK")){
                            if(!jsonobj.getBoolean("OK"))
                                Tools().showdialog(this@RegisterActivity,jsonobj.getString("error"))
                        }
                     }
                    override fun onfailure(error: String) {
                        d("jemso",error)
                     }
                },this)
            }
        }

        loginlinlk.setOnClickListener(){
            val ingtent=Intent(this, LoginActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(ingtent)
            overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out)
        }
    }
}

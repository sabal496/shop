package com.example.shop.activityes

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.View.OnFocusChangeListener
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.shop.R

import com.example.shop.databinding.ActivityCompleteProfileBinding
import com.example.shop.tools.Tools
import com.example.shop.viewmodel.MAinViewModel
import kotlinx.android.synthetic.main.activity_complete_profile.*
import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import androidx.annotation.RequiresApi
import kotlinx.coroutines.MainScope
import java.security.Permission



class CompleteProfileActivity : AppCompatActivity() {
    companion object {
        val REQUEST_CODE = 1
        val PERMISSIONS_REQUEST_CODE=10
    }

    lateinit var model: MAinViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //   setContentView(R.layout.activity_complete_profile)
        model = ViewModelProvider(this)[MAinViewModel::class.java]
        val binding: ActivityCompleteProfileBinding = DataBindingUtil.setContentView(
            this,
            R.layout.activity_complete_profile
        )

        binding.model = model
        binding.lifecycleOwner = this
        init()
    }

    private fun init() {
        listeners()
    }

    private fun listeners() {
        datepicker.setOnClickListener {
            Tools().showpicker(datepicker, this)
        }
        datepicker.onFocusChangeListener = OnFocusChangeListener { _view, hasFocus ->
            if (hasFocus) {
                Tools().showpicker(datepicker, this)
            }
        }

        choosephoto.setOnClickListener() {
            if (hasReadExternalStorage() && hasWriteExternalStorage() && hasCameraPermission()) {
                choosephoto()
            } else
                RequestPermisiions()
        }

        model.input.observe(this, androidx.lifecycle.Observer {

        })
    }

    private fun hasReadExternalStorage() = ActivityCompat.checkSelfPermission(
        this,
        Manifest.permission.READ_EXTERNAL_STORAGE
    ) == PackageManager.PERMISSION_GRANTED

    protected fun hasWriteExternalStorage() = ActivityCompat.checkSelfPermission(
        this,
        Manifest.permission.WRITE_EXTERNAL_STORAGE
    ) == PackageManager.PERMISSION_GRANTED

    private fun hasCameraPermission() =
        ActivityCompat.checkSelfPermission(
            this,
            Manifest.permission.CAMERA
        ) == PackageManager.PERMISSION_GRANTED

    @RequiresApi(Build.VERSION_CODES.M)
    private fun RequestPermisiions() {
        requestPermissions(
            arrayOf(
                Manifest.permission.CAMERA,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE
            ),
            PERMISSIONS_REQUEST_CODE
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
            if(REQUEST_CODE== PERMISSIONS_REQUEST_CODE){
                if(grantResults.size>2){
                    if(grantResults[0]==PackageManager.PERMISSION_GRANTED)
                        choosephoto()
                }
            }
    }
    private fun choosephoto(){
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(
            intent,
            REQUEST_CODE
        )
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE) {
            val imageuri = data?.data
            choosetext.visibility = View.INVISIBLE
            image.setImageURI(imageuri)
        }
    }

}

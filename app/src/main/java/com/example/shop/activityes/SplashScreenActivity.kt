package com.example.shop.activityes

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.example.shop.R
import com.example.shop.sharedpreferences.Sharedpreference

class SplashScreenActivity : AppCompatActivity() {
    var runable= Runnable {
        init()
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_screen_activity)
        Handler().postDelayed(runable,2000)
    }
    private fun init(){
        val activity:Activity = if(Sharedpreference.instance().getstring(
                Sharedpreference.USER
            )!!.isEmpty()) LoginActivity()
            else DashboardActivity()
        val intent=Intent(this,activity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out)
    }

    override fun onStart() {
        Handler().postDelayed(runable,2000)
        super.onStart()

    }
    override fun onPause() {
        super.onPause()
        Handler().removeCallbacks(runable)
    }
 }

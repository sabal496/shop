package com.example.shop.tools

import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.view.WindowManager
import android.widget.EditText
import com.example.shop.App
import com.example.shop.R
import kotlinx.android.synthetic.main.activity_complete_profile.*
import kotlinx.android.synthetic.main.dialog_vwindow.*
import java.util.*
import java.util.regex.Pattern

class Tools {
    fun isEmailValid(email: String): Boolean {
        return Pattern.compile(
            "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9]))|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
        ).matcher(email).matches()
    }

      fun showdialog(context:Context,desc:String){
        val dialog= Dialog(context)
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)
        dialog.window?.requestFeature(Window.FEATURE_NO_TITLE)

        dialog.setContentView(R.layout.dialog_vwindow)

        val params: ViewGroup.LayoutParams=dialog.window!!.attributes
        params.width= ViewGroup.LayoutParams.MATCH_PARENT
        params.height= ViewGroup.LayoutParams.WRAP_CONTENT
        dialog.window!!.attributes=params as WindowManager.LayoutParams
          dialog.dialgdesc.text = desc
        dialog.dialogbtn.setOnClickListener(){
            dialog.dismiss()
        }
        dialog.show()
    }

      fun showpicker(view1:EditText,context:Context){
        val c = Calendar.getInstance()
        val year1 = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)


        val dpd = DatePickerDialog(context, DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
            if(month<10)
                view1.setText("$dayOfMonth , 0$month , $year ")
            else
                view1.setText("$dayOfMonth , $month , $year ")
        }, year1, month, day)
        dpd.show()
    }
}
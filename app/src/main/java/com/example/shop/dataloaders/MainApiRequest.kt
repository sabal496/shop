package com.example.shop.dataloaders

import android.content.Context
import android.provider.Settings.Global.getString
import android.util.Log
import android.util.Log.d
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.example.shop.R

import com.example.shop.interfaces.CallbackApi
import com.example.shop.sharedpreferences.Sharedpreference
import okhttp3.OkHttpClient
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit

object MainApiRequest {
    private const val CODE_200=200
    private const val CODE_201=201
    private const val CODE_400=400
    private const val CODE_401=401
    private const val CODE_404=404
    private const val CODE_500=500
    private const val CODE_204=204

    private val httpClient = OkHttpClient.Builder()
        .connectTimeout(1, TimeUnit.MINUTES)
        .readTimeout(30, TimeUnit.SECONDS)
        .writeTimeout(15, TimeUnit.SECONDS)
        .addInterceptor { chain ->
            val token = Sharedpreference.instance().getstring(Sharedpreference.TOKEN)!!
            val request = chain.request().newBuilder()
                .addHeader("Content-Type", "application/json")
            if (token.isNotEmpty())
                request.addHeader("Authorization", "Bearer $token")
            chain.proceed(request.build())
        }

    var mainapi = Retrofit.Builder()
        .addConverterFactory(ScalarsConverterFactory.create())
        .baseUrl("https://ktorhighsteaks.herokuapp.com/")
        .client(httpClient.build())
        .build()

     var getadress=Retrofit.Builder().addConverterFactory(ScalarsConverterFactory.create())
         .baseUrl("https://maps.googleapis.com/maps/api/").build()

    var service = mainapi.create(
        Apiservice::class.java)



    val REGISTER="register"
    val LOGIN="login"

    fun postRequest(path :String, params:MutableMap<String,String>, callback: CallbackApi, context: Context){
        val call= service.Postquest(path,params)
        call.enqueue(
            calll(
                callback,
                context
            )
        )

    }
    private fun calll(callback: CallbackApi, context: Context) =  object : Callback<String> {
        override fun onFailure(call: Call<String>, t: Throwable) {
            callback.onfailure(t.message.toString())
        }

        override fun onResponse(call: Call<String>, response: Response<String>) {


            val responseCode=response.code()
            if(responseCode== CODE_200 ||responseCode== CODE_201){
                callback.onresponse(response.body().toString())
            }
            else if(responseCode== CODE_400){
                callback.onresponse(response.errorBody()!!.string())
            }
            else if(responseCode== CODE_401){
                callback.onresponse(response.errorBody()!!.string())

            }
            else if(responseCode== CODE_404){
                Toast.makeText(context,"user not found", Toast.LENGTH_SHORT).show()
            }
            else  if(responseCode== CODE_500){
                Toast.makeText(context,"server not responding", Toast.LENGTH_SHORT).show()

            }
            else if(responseCode== CODE_204){
                Toast.makeText(context,"no content", Toast.LENGTH_SHORT).show()

            }


        }

    }





    interface Apiservice {
        @FormUrlEncoded
        @POST("{path}")
        fun Postquest(@Path("path") path: String, @FieldMap params:Map<String,String>): Call<String>
    }
}
package com.example.shop.sharedpreferences

import android.content.Context
import android.content.SharedPreferences
import com.example.shop.App

class Sharedpreference {
    companion object{
        private var userdata: Sharedpreference?=null
        fun instance(): Sharedpreference {
            if (userdata ==null) userdata =
                Sharedpreference()
            return userdata!!
        }
        var USER_ID="userid"
        var TOKEN="token"
        var USER="user"
    }
    val sharedpreference:SharedPreferences by lazy {
        App.context!!.getSharedPreferences("userdata",Context.MODE_PRIVATE)
    }
    private val editor:SharedPreferences.Editor by lazy {
        sharedpreference.edit()
    }
    fun save(key:String,value:String){
        editor.putString(key,value)
        editor.apply()
    }
    fun getstring(key: String)=sharedpreference.getString(key,"")

    fun delete(key: String){
        if(sharedpreference.contains(key))
        editor.remove(key)
        editor.apply()
    }
    fun clear(){
        editor.clear()
        editor.apply()
    }

}
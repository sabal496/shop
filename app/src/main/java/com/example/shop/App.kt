package com.example.shop

import android.app.Application
import android.content.Context

class App:Application() {
    companion object{
        lateinit var instance:App
        lateinit var context:Context
    }

    override fun onCreate() {
        super.onCreate()
        instance=this
        context= applicationContext
    }
    fun getcontext()= context
}
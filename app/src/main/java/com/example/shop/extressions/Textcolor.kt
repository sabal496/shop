package com.example.shop.extressions

import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.widget.TextView

fun TextView.setColor(text:String,color:Int){
    val span=SpannableString(text)
    span.setSpan(ForegroundColorSpan(color),0,span.length,Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
    append(span)
}